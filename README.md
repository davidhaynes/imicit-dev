iMicIt Web App
==============

This is the official repository for the iMicIt web app.

It is built on Rails 3.2.1 for deployment to the Heroku cloud platform.

Platform
--------

The development environment matches as closely as possible the Heroku production environment, and comprises the following:

* pg
* thin
* gravatar_image_tag
* jquery

Development:

* Annotate (for automatic annotation of model code)

Testing:

* RSpec
* Spork
* Capybara
* Factory Girl

Asset pipeline:

* Sass
* Coffeescript
* Uglifier

Developed By
------------

**Machindo Apps**

+ http://machindoapps.com
+ http://bitbucket.org/machindoapps
+ http://twitter.com/machindo

License
-------

Licensed under the Apache License, Version 2.0: http://www.apache.org/licenses/LICENSE-2.0

