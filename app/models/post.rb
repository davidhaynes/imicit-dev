class Post < ActiveRecord::Base
  attr_accessible :content
  belongs_to :user

  validates :content, presence: true, length: { maximum: 63206 }
  validates :user_id, presence: true 

  default_scope order: 'posts.created_at DESC'

  def all_posts
    Post.where("user_id > 0")
  end
end
