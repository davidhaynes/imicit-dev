class PagesController < ApplicationController
  def home
    @title = "Home"
    if signed_in?
      @post = current_user.posts.build if signed_in?
      @feed_items = current_user.feed.paginate(page: params[:page])
    else
      @feed_items = Post.where("user_id > 0").paginate(page: params[:page])
    end
  end

  def contact
    @title = "Contact"
  end

  def about
    @title = "About"
  end

  def help
    @title = "Help"
  end
end

